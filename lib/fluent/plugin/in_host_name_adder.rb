require "fluent/plugin/version"
require "socket"

=begin
module Fluent
  module Plugin
    module Host
      module Name
        module Adder
          # Your code goes here...
        end
      end
    end
  end
end
=end

class InHostNameAdder < Fluent::TailInput
  Fluent::Plugin.register_input('in_host_name_adder', self)

  # Override 'configure_parser(conf)' method.
  # You can get config parameters in this method.
  def configure_parser(conf)
    @time_format = conf['time_format'] || '%Y-%M-%d %H:%M:%S'
  end

  # Override 'parse_line(line)' method that returns time and record.
  # This example method assumes following log format:
  #   %Y-%m-%d %H:%M:%S\tkey1\tvalue1\tkey2\tvalue2...
  #   %Y-%m-%d %H:%M:%S\tkey1\tvalue1\tkey2\tvalue2...
  #   ...
  def parse_line(line)
    elements = line.split("\t")

    time = elements.shift
    time = Time.strptime(time, @time_format).to_i

    # [k1, v1, k2, v2, ...] -> {k1=>v1, k2=>v2, ...}
    record = {}
    while (k = elements.shift) && (v = elements.shift)
      record[k] = v
    end

    return time, record
  end

  private

  def internal_ip_address
    hostent = Socket.gethostbyname(ENV["HOSTNAME"])
    hostent[3].unpack("C4").join('.')
  end
end
