# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fluent/plugin/version'

Gem::Specification.new do |spec|
  spec.name          = "fluent-plugin-host-name-adder"
#  spec.name          = "fluent-plugin-internal-ip-adder"
  spec.version       = Fluent::Plugin::Host::Name::Adder::VERSION
  spec.authors       = ["saber_chica"]
  spec.email         = ["saber.chica@gmail.com"]
  spec.description   = %q{TODO: Write a gem description}
  spec.summary       = %q{TODO: Write a gem summary}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  gem.add_development_dependency "fluentd"
  gem.add_runtime_dependency "fluentd"
end
